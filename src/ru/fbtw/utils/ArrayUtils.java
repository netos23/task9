package ru.fbtw.utils;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Версия 0.1
 * Ссылка на github: https://github.com/netos23/utils
 */
public class ArrayUtils {

	private static final String DEFAULT_ARR_READ_MSG = "Введите массив ";
	private static final String DEFAULT_SPLIT = " ";
	private static String delimiter = " ";

	private static Random random = new Random();

	// TODO: 06.11.2020 Добавить функции для других типов
	public static int[] readIntArray(String messageSuffix) {
		if (messageSuffix == null || messageSuffix.isEmpty()) {
			messageSuffix = DEFAULT_ARR_READ_MSG;
		} else {
			messageSuffix = DEFAULT_ARR_READ_MSG + messageSuffix;
		}

		Scanner in = new Scanner(System.in);

		while (true) {
			try {
				System.out.println(messageSuffix);
				System.out.println("Для разделения чисел используйте пробел");
				String arrayString = in.nextLine();

				return stringToIntArray(arrayString);
			} catch (NumberFormatException ex) {
				System.out.println("Возникла ошибка попробуйте еще раз");
			}
		}

	}

	// TODO: 06.11.2020 Добавить функции для других типов
	private static int[] stringToIntArray(String data) throws NumberFormatException {
		return Arrays.stream(data.split(delimiter))
				.mapToInt(Integer::parseInt)
				.toArray();
	}

	public static <T> void printArray(T[] array, String delimiter) {
		if (delimiter == null) {
			delimiter = DEFAULT_SPLIT;
		}

		System.out.println();
		for (T t : array) {
			System.out.printf("%1$s%2$s", t, delimiter);
		}
	}

	// TODO: 06.11.2020 Добавить функции для других типов
	public static void printPrimitiveArray(int[] array, String delimiter) {
		if (delimiter == null) {
			delimiter = DEFAULT_SPLIT;
		}

		System.out.println();
		for (int num : array) {
			System.out.printf("%1$s%2$s", num, delimiter);
		}
	}


	public static int[] randomArray(int length) {
		assert (length > 0);


		int[] array = new int[length];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt();
		}

		return array;
	}

	public static int[] randomArray(int length, int bound) {

		int[] array = new int[length];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(bound);
		}

		return array;
	}

	public static boolean[] toPrimitive(Boolean[] array) {
		boolean[] res = new boolean[array.length];

		for (int i = 0; i < res.length; i++) {
			res[i] = array[i];
		}

		return res;
	}


	public static <T> boolean matrixCopy(T[][] source, T[][] target) {

		if (source == null || target == null) {
			return false;
		}

		if (!(isRectMatrix(source) && isRectMatrix(target))) {
			return false;
		}

		int rowCount = Math.min(source.length, target.length);
		int colCount = Math.min(source[0].length, target[0].length);

		for (int r = 0; r < rowCount; r++) {
			System.arraycopy(source, 0, target, 0, colCount);
		}
		return true;
	}

	public static boolean matrixCopy(boolean[][] source, boolean[][] target) {

		if (source == null || target == null) {
			return false;
		}

		if (!(isRectMatrix(source) && isRectMatrix(target))) {
			return false;
		}

		int rowCount = Math.min(source.length, target.length);
		int colCount = Math.min(source[0].length, target[0].length);

		for (int r = 0; r < rowCount; r++) {
			for (int c = 0; c < colCount; c++) {
				target[r][c] = source[r][c];
			}
		}
		return true;
	}

	public static <T> boolean isRectMatrix(T[][] array) {
		int size = array[0].length;
		for (T[] row : array) {
			if (row.length != size) {
				return false;
			}
		}
		return true;
	}

	public static boolean isRectMatrix(boolean[][] array) {
		int size = array[0].length;
		for (boolean[] bool : array) {
			if (bool.length != size) {
				return false;
			}
		}
		return true;
	}
}
