package ru.fbtw.utils;

public class IntVector2 {
	private int x;
	private int y;

	public IntVector2(int x, int y) {
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof IntVector2){
			IntVector2 other = (IntVector2) obj;
			return this.x == other.x && this.y == other.y;
		}else{
			return false;
		}

	}
}
