package ru.fbtw.task9.core;

import java.util.Comparator;
import java.util.Map;

public class CustomComparator
        implements Comparator<Map.Entry<Integer, Integer>> {

    private static final CustomComparator instance = new CustomComparator();

    public static CustomComparator getInstance() {
        return instance;
    }

    private CustomComparator(){
    }

    /**
     * Сначала сравниваем количество переменных, а потом их значение
     * */
    @Override
    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
        if (!o2.getValue().equals(o1.getValue())) {
            return Integer.compare(o2.getValue(), o1.getValue());
        } else {
            return Integer.compare(o1.getKey(), o2.getKey());
        }
    }
}
