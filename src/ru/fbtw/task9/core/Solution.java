package ru.fbtw.task9.core;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Solution {
	private List<Integer> input;


	public List<Integer> run(List<Integer> input) {
		this.input = input;
		List<Map.Entry<Integer,Integer>> numPairs = countOf();
		numPairs.sort(CustomComparator.getInstance());

		return extractResult(numPairs);
	}

	private List<Map.Entry<Integer,Integer>> countOf(){
		Map<Integer, Integer> numsCount = new HashMap<>();

		for (Integer key : input) {
			if (numsCount.containsKey(key)) {
				int newValue = numsCount.get(key) + 1;
				numsCount.replace(key, newValue);
			} else {
				numsCount.put(key, 1);
			}
		}

		return new ArrayList<>(numsCount.entrySet());
	}

	private List<Integer> extractResult(List<Map.Entry<Integer,Integer>> sortedPairs){
		List<Integer> result = new ArrayList<>();

		for(Map.Entry<Integer,Integer> numCountPair : sortedPairs){
			for(int i = 0; i < numCountPair.getValue();i++){
				result.add(numCountPair.getKey());
			}
		}

		return result;
	}
}
