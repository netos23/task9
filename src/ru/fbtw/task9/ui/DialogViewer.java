package ru.fbtw.task9.ui;

import javafx.scene.control.Alert;

import java.io.File;

public class DialogViewer {

	public static void showFilePickDialog(
			String title,
			OnSumbitListner<File> onSubmitListener,
			String initVal,
			FileType fileType
	){
		new FileChoseDialog(title, onSubmitListener, initVal,fileType).execute();
	}


	public static void showError(String title, String content){
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(title);
		alert.setContentText(content);
		alert.show();
	}

}
