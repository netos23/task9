package ru.fbtw.task9;


import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import ru.fbtw.task9.core.Solution;
import ru.fbtw.task9.io.ExceptionMessages;
import ru.fbtw.task9.io.SolutionPrinter;
import ru.fbtw.task9.io.SolutionReader;
import ru.fbtw.task9.ui.DialogViewer;
import ru.fbtw.task9.ui.FileType;
import ru.fbtw.utils.ArrayUtils;
import ru.fbtw.utils.IntVector2;
import ru.fbtw.utils.cmd.ConsoleArgInputProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


public class Main extends Application {
	private static final double CELL_WIDTH = 50;
	private static final double CELL_HEIGHT = 50;
	private static final Color UNSELECTED_COLOR = Color.GRAY;
	private static final Color SELECTED_COLOR = Color.BLACK;
	private static final Color RESULT_COLOR = Color.RED;
	private static final int MIN_WIDTH = 650;
	private static final int MIN_HEIGHT = 400;

	/**
	 * Класс содержащий реализацию логики работы програмы
	 */
	private static Solution solver;

	private static PrintStream output = System.out;
	private static Scanner input = new Scanner(System.in);
	private static boolean isHeadless = false;

	private static boolean isConsoleInput = true, isConsoleOutput = true;

	private static SolutionPrinter printer;
	private static SolutionReader reader;
	private static List<Integer> inputData;

	private static int initWidth = 1;
	private static int initHeight = 1;

	private Label welcomeText;
	private TextField inputField;
	private Label result;
	private Button save,load,push;
	private boolean isShowResult;
	private Scene scene;


	/**
	 * Метод переопределяет ввод с консоли на ввод с файла
	 *
	 * @param args - в параметре 0 содержиться путь к файлу который следует загрузить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setInput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleInput = false;

			if (validateFileName(filename)) {

				File inputFile = new File(filename);
				input = new Scanner(inputFile);
			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}

	}

	/**
	 * @param fileName - имя файло которое следует проверить
	 * @return - возвращает true если фаил является текстовым
	 */
	private static boolean validateFileName(String fileName) {
		return fileName.endsWith(".txt");
	}

	/**
	 * Метод переопределяет вывод в консоли на вывод в файл
	 *
	 * @param args - в параметре 0 содержиться путь к файлу в который следует сохранить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setOutput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleOutput = false;

			if (validateFileName(filename)) {
				File outputFile = new File(filename);
				output = new PrintStream(outputFile);

			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}
	}

	/**
	 * Отключает оконный интерфейс
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void setIsHeadless(String... args) {
		isHeadless = true;
	}

	/**
	 * Просмотр помощи
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void getHelp(String... args) {
		String message = "Добро пожаловать в программу сортировки необычных листов"+
				"список команд: \n" +
				"--headless  позволяет отключить оконный интерфейс\n" +
				"--help запросить помощь\n" +
				"-i [dir] указать деррикторию из которой следует загрузить данные,\n" +
				"\tесли в headless режиме флаг не указан, то ввод необходимо произвести с консоли\n" +
				"-o [dir] указать деррикторию в которую следует сохранить данные,\n" +
				"\tесли в headless режиме флаг не указан, то вывод будет произведен в консоль\n";
		System.out.println(message);
		System.exit(0);
	}

	/**
	 * Инициализация программы
	 *
	 * @param args - параметры запуска программы из консоли
	 */
	public static void main(String[] args) {
		solver = new Solution();

		ConsoleArgInputProcessor processor = new ConsoleArgInputProcessor();

		processor.setFlagEvent("--help", Main::getHelp)
				.setFlagEvent("--headless", Main::setIsHeadless)
				.setFlagEvent("-i", Main::setInput)
				.setFlagEvent("-o", Main::setOutput);

		try {
			processor.execute(args);

			printer = new SolutionPrinter(output, isConsoleOutput);
			reader = new SolutionReader(input, isConsoleInput);

			if (isHeadless) {
				showWelcomeMessage();

				inputData = reader.readInput();
				List<Integer> result = solver.run(inputData);

				if (result == null) {
					result = Collections.EMPTY_LIST;
				}

				printer.printSolution(inputData, result);

				System.exit(0);
			} else {

				if (processor.hasFlag("-i")) {
					read();
				}
				launch(args);

			}
		} catch (Exception ex) {
			//ex.printStackTrace();
			String message = ex.getMessage();
			if (message == null) message = ExceptionMessages.UNKNOWN;
			System.err.println(message);
			System.exit(-1);
		}
	}

	/**
	 * При использовании оконного интерфейса метод загружает {@code inputData}
	 */
	private static void read() {
		inputData = reader.readInput();

	}


	/**
	 * При исспользовании консоли как средства ввода, выводит приветсвенное сообщение
	 */
	private static void showWelcomeMessage() {
		String message = "Добро пожаловать в программу поиска прямоугольников\n";

		if (isConsoleInput) {
			output.println(message);
		}
	}

	/**
	 * Задание разметки и контента для сцены {@param primaryStage}
	 */
	@Override
	public void start(Stage primaryStage) {
		isShowResult = false;
		primaryStage.setTitle("Поиск прямоугольников");

		VBox mainLayout = new VBox();

		welcomeText = new Label("Введите через пробел числа");
		inputField = new TextField();
		result = new Label("");

		GridPane footer = buildFooter();


		mainLayout.getChildren().addAll(
			welcomeText,
			inputField,
			result,
			footer
		);

		primaryStage.setMinWidth(MIN_WIDTH);
		primaryStage.setMinHeight(MIN_HEIGHT);

		scene = new Scene(mainLayout, MIN_WIDTH, MIN_HEIGHT);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Метод задает стили, колбеки и разметку нижней части интерфейса
	 *
	 * @return - разметку нижней части интерфейса
	 */
	private GridPane buildFooter() {

		Button pushBtn = new Button("Решить");
		pushBtn.setOnAction(this::find);

		Button saveBtn = new Button("Сохранить");
		saveBtn.setOnAction(this::saveOnClick);

		Button loadBtn = new Button("Загрузить");
		loadBtn.setOnAction(this::loadOnClick);

		Button clearBtn = new Button("Отчистить");
		clearBtn.setOnAction(this::clear);

		GridPane footerLayout = new GridPane();


		footerLayout.setPadding(new Insets(10.0));
		footerLayout.setVgap(10.0);
		footerLayout.setHgap(30.0);
		footerLayout.setAlignment(Pos.CENTER);


		footerLayout.add(pushBtn, 0, 0);
		footerLayout.add(saveBtn, 1, 0);
		footerLayout.add(loadBtn, 2, 0);
		footerLayout.add(clearBtn, 3, 0);


		return footerLayout;
	}

	private void clear(ActionEvent actionEvent) {
		inputField.clear();
	}



	/**
	 * Метод обратного вызова при нажатии на кнопку загрузки файла
	 *
	 * @param event - параметр метода функционального интерфейса
	 */
	private void loadOnClick(ActionEvent event) {
		DialogViewer.showFilePickDialog("Загрузить", this::load_file, "", FileType.LOAD);
	}


	/**
	 * Загружает фаил и обрабатывает ошибки при его открытии
	 *
	 * @param file - фаил который был выбран пользователем в диалоговом окне
	 */
	private void load_file(File file) {
		if (file != null) {
			try {
				Scanner in = new Scanner(file);
				reader.setInputSource(in, false);
				read();

				String input = printer.toString(inputData);
				inputField.setText(input);
			} catch (FileNotFoundException e) {
				System.err.println(ExceptionMessages.FILE_ERR);
				DialogViewer.showError("Ошибка", ExceptionMessages.FILE_ERR);
			} catch (Exception ex) {
				String msg = ex.getMessage();
				if (msg == null) msg = ExceptionMessages.UNKNOWN;
				System.err.println(msg);
				DialogViewer.showError("Ошибка", msg);
			}
		}
	}

	/**
	 * Метод обратного вызова при нажатии на кнопку сохранения файла
	 *
	 * @param event - параметр метода функционального интерфейса
	 */
	private void saveOnClick(ActionEvent event) {
		DialogViewer.showFilePickDialog("Сохранить", this::save_file, "", FileType.SAVE);
	}

	private void save_file(File file) {
		try {
			PrintStream out = new PrintStream(file);
			printer.setOutputSource(out);

			List<Integer> result = solver.run(inputData);
			printer.printSolution(inputData, result);
		} catch (FileNotFoundException e) {
			System.err.println(ExceptionMessages.FILE_ERR);
			DialogViewer.showError("Ошибка", ExceptionMessages.FILE_ERR);
		} catch (Exception ex) {
			System.err.println(ExceptionMessages.UNKNOWN);
			DialogViewer.showError("Ошибка", ExceptionMessages.UNKNOWN);
		}
	}

	/**
	 * Запускает поиск прямоугольников
	 *
	 * @param event - параметр метода функционального интерфейса
	 * @return - результат работы программы
	 */
	private List<Integer> find(ActionEvent event) {
		inputData = reader.fromString(inputField.getText());
		List<Integer> result = solver.run(inputData);

		if (result == null) {
			result = Collections.EMPTY_LIST;
		} else {
			showResult(result);
		}

		return result;
	}

	/**
	 * Скрывает временно найденный результат
	 */
	private void clearResult() {
		if (isShowResult) {
			result.setText("");
			isShowResult = false;
		}
	}

	/**
	 * Временно показывает результат
	 *
	 * @param res - результат работы
	 */
	private void showResult(List<Integer> res) {
		isShowResult = true;
		String resText = printer.toString(res);
		result.setText(resText);
	}
}
