package ru.fbtw.task9.io;



import java.awt.*;
import java.io.PrintStream;
import java.util.List;

public class SolutionPrinter {
	private static final String INPUT_INFO = "Для данных:\n";
	private static final String RESULT_INFO = "\nРезультат:\n";
	private PrintStream out;
	private boolean isConsole;


	public SolutionPrinter(PrintStream output) {
		this(output, System.out == output);
	}

	public SolutionPrinter(PrintStream output, boolean isConsole) {
		this.out = output;
		this.isConsole = isConsole;
	}


	/**
	 * @param solution - решение
	 * @param input - входные данные
	 */
	public void printSolution(List<Integer> input, List<Integer> solution) {
		out.println(INPUT_INFO);
		printList(input);
		out.println(RESULT_INFO);
		printList(solution);
	}

	/**
	 * @param list - входная матрица
	 */
	private void printList(List<Integer> list) {
		list.forEach(item -> out.printf("%d ",item));
	}

	public String toString(List<Integer> list){
		StringBuilder builder = new StringBuilder();
		list.forEach(item -> builder.append(item).append(' '));

		return builder.toString();
	}


	/**
	 * Устанавливает поток вывода
	 *
	 * @param out - новый поток вывода
	 */
	public void setOutputSource(PrintStream out) {
		this.out = out;
	}
}
