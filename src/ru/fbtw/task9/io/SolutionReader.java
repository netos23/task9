package ru.fbtw.task9.io;

import ru.fbtw.utils.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;

public class SolutionReader {
    private static final String err = "Возникла ошибка попробуйте еще раз";
    private Scanner scanner;
    private boolean isConsole;

    public SolutionReader(Scanner input, boolean isConsole) {
        this.scanner = input;
        this.isConsole = isConsole;
    }

    /**
     * Метод взависимости от входного потока считывает входную матрицу
     *
     * @return - входная матрица
     */
    public List<Integer> readInput() {
        return isConsole ? readFromConsole() : readFromFile();
    }

    private List<Integer> readFromConsole() {
        int len;
        len = readInt("Ввидите длину(целое число):");

        final String msg = String.format("Введите лист длинной: %d, для разделения используйте пробел", len);
        System.out.println(msg);

        while (true) {
            try {
                return readListBySize(len);
            } catch (Exception ex) {
                System.out.println(err);
            }
        }
    }

    private int readInt(String msg) {
        int val;
        while (true) {
            System.out.println(msg);
            try {
                val = scanner.nextInt();
                break;
            } catch (Exception ex) {
                System.out.println(err);
            }
        }
        return val;
    }

    /**
     * Считывает лист из файла
     *
     * @return - входной лист
     */
    private List<Integer> readFromFile() {
        List<Integer> res = new ArrayList<>();
        try {
            while (scanner.hasNext()){
                String tmp = scanner.next();
                res.add(Integer.parseInt(tmp));
            }
        } catch (InputMismatchException ex) {
            throw new InputMismatchException(ExceptionMessages.INPUT_ERR);
        }

        return res;
    }

    /**
     * @param len - длина списка
     * @return - список считанный по заданной длине
     */
    private List<Integer> readListBySize(int len) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            res.add(scanner.nextInt());
        }
        return res;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setInputSource(Scanner scanner, boolean isConsole) {
        this.scanner = scanner;
        this.isConsole = isConsole;
    }

    public List<Integer> fromString(String text) {
        try {
            return Arrays.stream(text.split(" "))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
        }catch (Exception ex){

        }
        return Collections.EMPTY_LIST;
    }
}
